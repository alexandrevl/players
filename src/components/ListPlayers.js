import React, { useEffect, useState } from 'react';
import Table from './Table.js';
import styled from 'styled-components';
import { Container, Row, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

const Styles = styled.div`
	padding: 1rem;

	table {
		border-spacing: 0;
		border: 1px solid black;
		width: 800px;

		tr {
			:last-child {
				td {
					border-bottom: 0;
				}
			}
		}

		th,
		td {
			margin: 0;
			padding: 0.5rem;
			border-bottom: 1px solid black;
			border-right: 1px solid black;

			:last-child {
				border-right: 0;
			}
		}
	}
`;

export default function ListPlayers(props) {
	const [players, setPlayers] = useState([]);

	useEffect(() => {
		let url = 'https://localhost:21266/players/';
		if (window.location.hostname.includes('mrguinas')) {
			url = 'https://multistreamer.xyz:21266/players/';
		}
		console.log(url);
		fetch(`${url}${props.match.params.idLive}`)
			.then((res) => res.json())
			.then(
				(list) => {
					let result = [];
					console.log(list);
					list.teamUsers.team1.forEach((user) => {
						result.push({ img: <img src={user.profileImageUrl} width="45vw" alt={user.displayName} />, name: user.displayName, time: 'Vermes' });
					});
					list.teamUsers.team2.forEach((user) => {
						result.push({ img: <img src={user.profileImageUrl} width="45vw" alt={user.displayName} />, name: user.displayName, time: 'Pragas' });
					});
					setPlayers(result);
				},
				(error) => {}
			);
	}, [props.match.params.idLive]);
	return (
		<Container>
			<Row>
				<Col>
					<Styles>
						<Table
							columns={[
								{
									Header: '',
									accessor: 'img',
									maxWidth: 70,
									minWidth: 70,
									id: 'image',
									Cell: (row) => <div style={{ textAlign: 'center' }}>{row.value}</div>,
								},
								{ Header: 'Nome', accessor: 'name', id: 'name' },
								{ Header: 'Time', accessor: 'time', id: 'time' },
							]}
							data={players}
						/>
					</Styles>
				</Col>
			</Row>
		</Container>
	);
}
