import React from 'react';
import ListPlayers from './components/ListPlayers.js';
import Table from './components/Table.js';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

export default function App() {
	return (
		<>
			<BrowserRouter>
				<Switch>
					<Route path="/table/:idLive" component={Table} />
					<Route path="/cabo/:idLive" component={ListPlayers} />
				</Switch>
			</BrowserRouter>
		</>
	);
}
